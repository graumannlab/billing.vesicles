require(magrittr)

# Batch effect corrected data
#----------------------------
result_dir <- '../../../results/billing/billing.vesicles/injection_corrected'
eset_obj <- billing.vesicles::billing.vesicles %>% 
  (function(x){
    autonomics.eset::exprs(x) %<>% limma::removeBatchEffect(batch = x$injection )             
    x
  })
eset_obj$subgroup
contrasts <- c(CM_ESC  = 'CM  - ESC', 
               EX_ESC  = 'EX  - ESC', 
               MV_ESC  = 'MV  - ESC', 
               NCM_ESC = 'NCM - ESC', 
               KIT_ESC = 'KIT - ESC', 
               
               CM_NCM  = 'CM  - NCM',
               MV_NCM  = 'MV  - NCM',
               EX_NCM  = 'EX  - NCM',
               KIT_NCM = 'KIT - NCM')
autonomics::analyze_eset(eset_obj, result_dir = result_dir, contrasts = contrasts)

# Ratio + Batch correction
#-------------------------
result_dir <- '../../../results/billing/billing.vesicles/ratio_and_injection_corrected'
eset_obj <- billing.vesicles::billing.vesicles %>% 
            (function(x){
              autonomics.eset::exprs(x)[, 1:6]  %<>% magrittr::subtract(autonomics.eset::exprs(x)[, 1])
              autonomics.eset::exprs(x)[, 7:12] %<>% magrittr::subtract(autonomics.eset::exprs(x)[, 7])
              x %<>% magrittr::extract(, c(-1, -7))
              x$subgroup %<>% droplevels()
              x
            })  %>% 
            (function(x){
              autonomics.eset::exprs(x) %<>% limma::removeBatchEffect(batch = x$injection )             
              x
            })
eset_obj$subgroup
contrasts <- c(CM_NCM  = 'CM  - NCM',
               MV_NCM  = 'MV  - NCM',
               EX_NCM  = 'EX  - NCM',
               KIT_NCM = 'KIT - NCM')
eset_obj %>% autonomics::analyze_eset(result_dir = result_dir, contrasts = contrasts)
